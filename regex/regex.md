# Expresiones regulares (REGEX)

## Descripción
**Punto "."** Representa cualquier caracter.

**Barra inversa "\\"** Escapar carateres especiales.

**Los corchetes "[ ]"** sirve para representar clases de caracteres. Ejemplo: "[a-z]" cualquier caracter de la a a la z en minúscula, "[a-zA-Z]" cualquier caracter de la a a la z en minúscula o mayúscula.

**La barra "|"** Una de varias expresiones (or lógico).

**El signo de dólar "$"** Final de línea.

**El acento circunflejo "^"** Inicio de cadena, cuando se utiliza dentro de clases es una negación.

**Los paréntesis "()"** Realizar agrupaciones que posteriormente se pueden indexar.  Para nombrar las agupaciones se puede utilizar ?<nombre>. Ejemplo: "^(?<Día>\d\d)\/(?<Mes>\d\d)\/(?<Año>\d\d\d\d)$"

**El signo de interrogación "?"** Cero o una ocurrencia de una expresión.


## Metacaracteres para repeticiones

**Las llaves "{}"** Cantidad de repeticiones de la expresión a encontrar. Ejemplo: "\d{2}" dos número, "\d{2,10}" de dos a diez números.

**El asterisco "*"** Cero o mas repeticiones.

**El signo de suma "+"** Una o mas repeticiones.


## Caracteres especiales
    \t — Representa un tabulador.

    \r — Representa el "retorno de carro" o "regreso al inicio" o sea el lugar en que la línea vuelve a iniciar.

    \n — Representa la "nueva línea" el carácter por medio del cual una línea da inicio. Es necesario recordar que en Windows es necesaria una combinación de \r\n para comenzar una nueva línea, mientras que en Unix solamente se usa \n y en Mac_OS clásico se usa solamente \r.

    \a — Representa una "campana" o "beep" que se produce al imprimir este carácter.

    \e — Representa la tecla "Esc" o "Escape"

    \f — Representa un salto de página

    \v — Representa un tabulador vertical

    \x — Se utiliza para representar caracteres ASCII o ANSI si conoce su código. De esta forma, si se busca el símbolo de derechos de autor y la fuente en la que se busca utiliza el conjunto de caracteres latín-1 es posible encontrarlo utilizando \xA9".

    \u — Se utiliza para representar caracteres Unicode si se conoce su código. "\u00A2" representa el símbolo de centavos. No todos los motores de Expresiones Regulares soportan Unicode. El .Net Framework lo hace, pero el EditPad Pro no, por ejemplo.

    \d — Representa un dígito del 0 al 9.

    \w — Representa cualquier carácter alfanumérico.

    \s — Representa un espacio en blanco.

    \D — Representa cualquier carácter que no sea un dígito del 0 al 9.

    \W — Representa cualquier carácter no alfanumérico.

    \S — Representa cualquier carácter que no sea un espacio en blanco.

    \A — Representa el inicio de la cadena. No un carácter sino una posición.

    \Z — Representa el final de la cadena. No un carácter sino una posición.

    \b — Marca la posición de una palabra limitada por espacios en blanco, puntuación o el inicio/final de una cadena.

    \B — Marca la posición entre dos caracteres alfanuméricos o dos no-alfanuméricos.
